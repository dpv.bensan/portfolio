    const secondHand = document.querySelector('.second-hand');

    const minHand = document.querySelector('.min-hand');
    
    const hourHand = document.querySelector('.hour-hand');

    function setDate(){

      const now = new Date();
      const seconds = now.getSeconds();
      const secondsDegrees = ((seconds / 60) * 360) + 90;
      secondHand.style.transform = `rotate(${secondsDegrees}deg)`;

      const mins = now.getMinutes();
      const minsDegrees = ((mins / 60) * 360) + 90;
      minHand.style.transform = `rotate(${minsDegrees}deg)`;

      const hours = now.getHours();
      const hoursDegrees = ((hours / 12) * 360) + 90;
      hourHand.style.transform = `rotate(${hoursDegrees}deg)`;
   
      const hands = document.querySelectorAll('.hand');
        if (seconds === 0) {
        hands.forEach(hand => hand.style.transitionDuration = '0s');
          }else{
        hands.forEach(hand => hand.style.transitionDuration = '0.05s');
          }
      
          const redherrings = document.querySelectorAll('.redherring');
          if (seconds === 0) {
            redherrings.forEach(redherring => redherring.style.color = 'red' );
          }else{
            redherrings.forEach(redherring => redherring.style.color = 'white' );
          }
   }
    setInterval(setDate, 1000);